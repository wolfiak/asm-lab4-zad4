.386
.MODEL flat,STDCALL

STD_INPUT_HANDLE equ -10
STD_OUTPUT_HANDLE equ -11

GetStdHandle PROTO :DWORD
ExitProcess PROTO : DWORD
wsprintfA PROTO C :VARARG
WriteConsoleA PROTO : DWORD, :DWORD, :DWORD, :DWORD, :DWORD
ReadConsoleA  PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
atoi  PROTO :DWORD
lstrlenA PROTO :DWORD

.DATA
		cout		   dd ?
		cin			   dd ?
		tekst          db "Wprowadz a: ",0
		rozmiart       dd $ - tekst
		tekst2         db "Wprowadz b: ",0
		rozmiart2      dd $ - tekst2
		tekst3         db "Wprowadz c: ",0
		rozmiart3      dd $ - tekst3
		tekst4         db "Wprowadz d: ",0
		rozmiart4      dd $ - tekst4
		liczba         dd 0
		liczbaW		   db "Wynik (a+b)*c/d  to: %i ",0
		rliczbaW	   dd $ - liczbaW
		wynik          dw 128  DUP(0)
		bufor          dw 128 DUP(0)
		bufor2         dw 128 DUP(0)
		bufor3         db 2 DUP(0)
		bufor4         db 2 DUP(0)
		zm             dd ?
		zmT			   dd ?
		zm2            dd ?
		zm3            dd ?
		zm4            dd ?
		liczbaZ        dd 0
		rozmiar		   dd ?
		teksto         db 128 DUP(0)
		buforbin	   db 8 DUP(0),0
		rbuf		   dd $ - buforbin
.CODE
main proc
	invoke GetStdHandle, STD_OUTPUT_HANDLE
	mov cout, EAX
	
	invoke GetStdHandle, STD_INPUT_HANDLE
	mov cin, EAX

	invoke WriteConsoleA, cout, OFFSET tekst, rozmiart, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor, 32, OFFSET liczba, 0
	LEA EBX, bufor
	mov EDI,liczba
	mov BYTE PTR [EBX+EDI-2],0
		push OFFSET bufor
		call ScanBin

	mov zm, EAX
	invoke WriteConsoleA, cout, OFFSET tekst2, rozmiart2, OFFSET liczba, 0

	invoke ReadConsoleA, cin, OFFSET bufor, 32, OFFSET liczbaZ, 0
	LEA EBX, bufor
	mov EDI,liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
		push OFFSET bufor
		call ScanBin
	mov zm2, EAX

	invoke WriteConsoleA, cout, OFFSET tekst3, rozmiart3, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor, 32, OFFSET liczbaZ, 0
	LEA EBX, bufor
	mov EDI,liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	push OFFSET bufor
	call ScanBin
	mov zm3, EAX
	
	invoke WriteConsoleA, cout, OFFSET tekst4, rozmiart4, OFFSET liczba, 0
	invoke ReadConsoleA, cin, OFFSET bufor2, 32, OFFSET liczbaZ, 0
	LEA EBX, bufor2
	mov EDI,liczbaZ
	mov BYTE PTR [EBX+EDI-2],0
	push OFFSET bufor
	call ScanBin
	mov zm4, EAX

	mov EAX, zm
	or EAX,zm2
	mov zm, EAX

	mov EAX, zm3
	xor EAX, zm4
	mov zm2, EAX


	MOV EAX,zm
	and EAX, zm2
mov ecx,8
	mov edi,OFFSET buforbin
L1:	shl al,1
	mov BYTE PTR [edi],'0' ;wstawiamy do bufora kod ASCII 0
	jnc L2 		;jump if not carry - je�eli cf=0 to skok do etykiety L2
	mov BYTE PTR [edi],'1' ;gdy cf=1 wstawiamy jednak do bufora kod 1
L2:	inc edi
	loop L1	;od ecx odejmujemy 1, je�eli nie 0 to skok do etykiety L1


;	invoke wsprintfA, OFFSET wynik, OFFSET liczbaW, zm
	invoke WriteConsoleA, cout, OFFSET buforbin, rliczbaW , OFFSET liczba, 0

	invoke ExitProcess, 0



main endp
atoi proc uses esi edx inputBuffAddr:DWORD
	mov esi, inputBuffAddr
	xor edx, edx
	xor EAX, EAX
	mov AL, BYTE PTR [esi]
	cmp eax, 2dh
	je parseNegative

	.Repeat
		
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0
	mov EAX, EDX
	jmp endatoi

	parseNegative:
	inc esi
	.Repeat
		lodsb
		.Break .if !eax
		imul edx, edx, 10
		sub eax, "0"
		add edx, eax
	.Until 0

	xor EAX,EAX
	sub EAX, EDX
	jmp endatoi

	endatoi:
	ret
atoi endp


ScanBin  PROC 
;; funkcja ScanInt przekszta�ca ci�g cyfr dziesi�tnych do liczby, kt�r� jest zwracana przez EAX 
;; argument - zako�czony zerem wiersz z cyframi 
;--- pocz�tek funkcji 
   push   EBP 
   mov   EBP, ESP   ; wska�nik stosu ESP przypisujemy do EBP 
;--- odk�adanie na stos 
   push   EBX 
   push   ECX 
   push   EDX 
   push   ESI 
   push   EDI 
;--- przygotowywanie cyklu 
   mov   EBX, [EBP+8] 
   push   EBX 
   call   lstrlenA 
   mov   EDI, EAX   ;liczba znak�w 
   mov   ECX, EAX   ;liczba powt�rze� = liczba znak�w 
   xor   ESI, ESI   ; wyzerowanie ESI 
   xor   EDX, EDX   ; wyzerowanie EDX 
   xor   EAX, EAX   ; wyzerowanie EAX 
   mov   EBX, [EBP+8] ; adres tekstu
;--- cykl -------------------------- 
pocz: 
   cmp   BYTE PTR [EBX+ESI], 0h   ;por�wnanie z kodem \0 
   jne   @F 
   jmp   et4 
@@: 
   cmp   BYTE PTR [EBX+ESI], 0Dh   ;por�wnanie z kodem CR 
   jne   @F 
   jmp   et4 
@@: 
   cmp   BYTE PTR [EBX+ESI], 0Ah   ;por�wnanie z kodem LF 
   jne   @F 
   jmp   et4 
@@: 
   cmp   BYTE PTR [EBX+ESI], 02Dh   ;por�wnanie z kodem - 
   jne   @F 
   mov   EDX, 1 
   jmp   nast 
@@: 
   cmp   BYTE PTR [EBX+ESI], 030h   ;por�wnanie z kodem 0 
   jae   @F 
   jmp   nast 
@@: 
   cmp   BYTE PTR [EBX+ESI], 031h   ;por�wnanie z kodem 9 
   jbe   @F 
   jmp   nast 
;---- 
@@:    
   push   EDX   ; do EDX procesor mo�e zapisa� wynik mno�enia 
   mov   EDI, 2 
   mul   EDI      ;mno�enie EAX * EDI 
   mov   EDI, EAX   ; tymczasowo z EAX do EDI 
   xor   EAX, EAX   ;zerowani EAX 
   mov   AL, BYTE PTR [EBX+ESI] 
   sub   AL, 030h   ; korekta: cyfra = kod znaku - kod 0    
   add   EAX, EDI   ; dodanie cyfry 
   pop   EDX 
nast:   
   inc   ESI 
   loop   pocz 
;--- wynik 
   or   EDX, EDX   ;analiza znacznika EDX 
   jz   @F 
   neg   EAX 
@@:    
et4:;--- zdejmowanie ze stosu 
   pop   EDI 
   pop   ESI 
   pop   EDX 
   pop   ECX 
   pop   EBX 
;--- powr�t 
   mov   ESP, EBP   ; przywracamy wska�nik stosu ESP
   pop   EBP 
   ret	4
ScanBin   ENDP 
END